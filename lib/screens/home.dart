import 'package:flutter/material.dart';
import 'web_view_container.dart';

class Home extends StatelessWidget {
  static final links = ['https://leonidas-online.ro'];
  List<String> url = ['https://leonidas-online.ro/ro/configurator-ballotin', 'https://leonidas-online.ro/ro/cutii-praline-1', 'https://leonidas-online.ro/ro/ballotin-34', 'https://leonidas-online.ro/ro/confiserie-2', 'https://leonidas-online.ro/ro/gourmet-3','https://leonidas-online.ro/ro/bauturi-4','https://leonidas-online.ro/ro/cadouri-33','https://leonidas-online.ro/ro/promo'];
  List<String> names = ['Configurator Ballotin', 'Cutii Praline', 'Ballotin', 'Confiserie', 'Gourmet','Bauturi','Cadouri','Promotii'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Leonidas - Ciocolata belgiana", 
        style: TextStyle(fontFamily: 'Lobster',fontSize: 25.0))),
        key: Key('123'),
        drawer:Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.brown[800]
          ),
         child:Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue[900],
                  image: DecorationImage(
                    image: AssetImage('assets/img/2.png'),
                    fit: BoxFit.cover)),
              ),
              ListTile(
                title: Text('Praline',
        style: TextStyle(fontFamily: 'Lobster',fontSize: 22.0, color:Colors.white)),
                onTap: () => gotopage(
                    context, 'https://leonidas-online.ro/ro/praline-56'),
                trailing: Icon(Icons.room_service, color:Colors.white),
              ),
              ListTile(
                title: Text('Companie',
        style: TextStyle(fontFamily: 'Lobster',fontSize: 22.0, color:Colors.white)),
                onTap: () =>
                    gotopage(context, 'https://leonidas-online.ro/ro/franciza'),
                trailing: Icon(Icons.business_center, color:Colors.white),
              ),
              ListTile(
                title: Text('Contact',
        style: TextStyle(fontFamily: 'Lobster',fontSize: 22.0, color:Colors.white)),
                onTap: () =>
                    gotopage(context, 'https://leonidas-online.ro/ro/contact'),
                trailing: Icon(Icons.contact_mail, color:Colors.white),
              ),
              ListTile(
                title: Text(
                  'Contul tau',
        style: TextStyle(fontFamily: 'Lobster',fontSize: 22.0, color:Colors.white)
                ),
                onTap: () => gotopage(
                    context, 'https://leonidas-online.ro/ro/auth/signup'),
                trailing: Icon(Icons.account_circle, color:Colors.white),
              ),
              ListTile(
                title: Text('Cosul tau',
        style: TextStyle(fontFamily: 'Lobster',fontSize: 22.0, color:Colors.white)),
                onTap: () => gotopage(
                    context, 'https://leonidas-online.ro/shopping-cart'),
                trailing: Icon(Icons.shopping_basket, color:Colors.white),
              ),
            ],
          ),
        )),
        body: 
        
        new Container(
          decoration: new BoxDecoration(
                image: new DecorationImage(
                    
                    image: new AssetImage('assets/img/1.png'),
                    fit: BoxFit.cover)),
        child: Center(

          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                   _urlButton(context, url[0],names[0]
                  ),
                  SizedBox(height: 15),
                   _urlButton(context, url[1],names[1]
                  ),
                  SizedBox(height: 15),
                   _urlButton(context, url[2],names[2]
                  ),
                  SizedBox(height: 15),
                  _urlButton(context, url[3],names[3]
                  ),
                  SizedBox(height: 15),
                   _urlButton(context, url[4],names[4]
                  ),
                  SizedBox(height: 15),
                   _urlButton(context, url[5],names[5]
                  ),
                  SizedBox(height: 15),
                   _urlButton(context, url[6],names[6]
                  ),
                  SizedBox(height: 15),
                  _urlButton(context, url[7],names[7]
                  ),
                  
                ],
              )
            ,
          ),
        ));
  }

  Widget _urlButton(BuildContext context, String url, String name) {
    return Container(
        height: 55.0,
        width: 240.0,
        padding: EdgeInsets.all(4.0),
        child: RaisedButton(
          color: Colors.brown[700],
          child: Text(name, style: TextStyle(fontSize: 24.0, fontFamily: 'Lobster',)),
          elevation: 3.0,
          splashColor: Colors.blueGrey,
          textColor: Colors.white,
          onPressed: () => _handleURLButtonPress(context, url),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(40.0)),
        ));
  }

  void _handleURLButtonPress(BuildContext context, String url) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => WebViewContainer(url)));
  }

  void gotopage(BuildContext context, String url) {
    _handleURLButtonPress(context, url);
  }
}
